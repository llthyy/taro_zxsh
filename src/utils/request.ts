import Taro from '@tarojs/taro'
import { logError,login } from '@/utils/common'



const baseUrl = process.env.BASEAPI;

 const HTTP_STATUS = {
    SUCCESS: 20000,
    AUTHENTICATE: 28004,
    PARAMS: 21003
  };

//   SUCCESS(true, 20000,"成功"),
//     UNKNOWN_REASON(false, 20001, "未知错误"),
//     PARAM_ERROR(false, 21003, "参数不正确"),
//     AUTHORIZATION_ERROR(false, 28004, "鉴权失败")

class request{

  get=function(url:string){
    const contentType = 'application/json';
    const method="GET";
    return new Promise((reslove, reject) => {
        Taro.request({
        url: baseUrl + url,
          method:method,
          header: {
            'content-type': contentType,
            'Authorization': Taro.getStorageSync('token')?Taro.getStorageSync('token'):""
          },
          success: (res:any) => {
            if (res.data.code === HTTP_STATUS.SUCCESS) {
                reslove(res.data.data)
              }else if (res.data.code === HTTP_STATUS.AUTHENTICATE) {
              Taro.clearStorage()
              // Taro.navigateTo({
              //   url: '/pages/login/index'
              // })
              Taro.showModal({
                title:'提示!',
                cancelText:'取消',
                cancelColor:'xx',
                confirmText:'确认',
                confirmColor:'xx',
                content:'请先授权登录!',
                showCancel:true,
                success(res)
                {
                    if(res.confirm)
                    {
                      login();
                    }else if(res.cancel)
                    {

                    }
                }
            })

              return logError('api', '请先登录')
            } else if(res.data.code === HTTP_STATUS.PARAMS){
                return logError('api', '参数错误')
            }else{
              Taro.showToast({
                title: res.data.message,
                icon:"none",
                duration: 1500
              })
              reslove(res.data.data);
            }
          },
          fail: (msg) => {
            logError('api', '请求接口出现问题', msg)
          }
        })
      })
 }

 post=function(url:string,data?:object){
    const contentType = 'application/json';
    const method="POST";
    return new Promise((reslove, reject) => {
        Taro.request({
        url: baseUrl + url,
          method:method,
          data,
          header: {
            'content-type': contentType,
            'Authorization': Taro.getStorageSync('token')?Taro.getStorageSync('token'):""
          },
          success: (res:any) => {
              console.log(res,888)
            if (res.data.code === HTTP_STATUS.SUCCESS) {
                reslove(res.data.data)
              }else if (res.data.code === HTTP_STATUS.AUTHENTICATE) {
              Taro.clearStorage()
              Taro.navigateTo({
                url: '/pages/login/index'
              })
              return logError('api', '请先登录')
            } else if(res.data.code === HTTP_STATUS.PARAMS){
                return logError('api', '参数错误')
            }else{
              Taro.showToast({
                title: res.data.message,
                icon:"none",
                duration: 1500
              });
              reslove(res.data.data);
            }
          },
          fail: (msg) => {
            logError('api', '请求接口出现问题', msg)
          }
        })
      })
 }

}

const Request = new request()
export default Request
