import Taro from "@tarojs/taro";

export const rpx2px = (rpx: number): number => {
  let deviceWidth = Taro.getSystemInfoSync().windowWidth;
  let px = (deviceWidth / 750) * Number(rpx);
  return px;
};

export const px2rpx = (px: number): number => {
  let deviceWidth = Taro.getSystemInfoSync().windowWidth;
  let rpx = (750 / Number(px)) * deviceWidth;
  return rpx;
};


export const timestampToTime = (atime: string):string => {
  let byTime = [365 * 24 * 60 * 60 * 1000, 24 * 60 * 60 * 1000, 60 * 60 * 1000, 60 * 1000, 1000];
  let unit = ["年", "天", "小时", "分钟", "秒钟"];
  let ct: any = Number(new Date().getTime()) - new Date(atime).getTime();
  if (ct < 0) {
    return "瞎糊闹！"
  }
  let sb: any[] = [];
  for (let i = 0; i < byTime.length; i++) {
    if (ct < byTime[i]) {
      continue;
    }
    let temp = Math.floor(ct / byTime[i]);
    ct = ct % byTime[i];
    if (temp > 0) {
      sb.push(`${temp}${unit[i]}`);
    }
    /*一下控制最多输出几个时间单位：
      一个时间单位如：N分钟前
      两个时间单位如：M分钟N秒前
      三个时间单位如：M年N分钟X秒前
    以此类推
    */
    if (sb.length >= 1) {
      break;
    }
  }
  return sb.join("")+"前";
};


