import { BaseComponents, TabBar, NavBar } from "@/components";
import { View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import { AtActivityIndicator } from 'taro-ui'
import { TAB_BAR_HEIGHT } from "@/components/TabBar";
import {
  Props as NavBarProps,
  NAV_BAR_HEIGHT,
  defaultProps as NavBarDefaultProps,
} from "@/components/NavBar";

import styles from "./index.module.scss";

interface Props extends NavBarProps {
  tabBar: boolean;
  navBar: boolean;
  loadding:boolean;
}

interface State {
  tabBarNavBarHeight: number;
}

class Layout extends BaseComponents<Props, State> {
  static defaultProps = {
    tabBar: false,
    navBar: false,
    ...NavBarDefaultProps,
    loadding:false
  };

  constructor(props) {
    super(props);

    this.state = {
      tabBarNavBarHeight: 0,
    };
  }

  componentDidMount() {
    this.init();
  }

  init = () => {
    const { tabBar, navBar } = this.props;
    const tabbarH = tabBar ? TAB_BAR_HEIGHT : 0;
      const navbarH = navBar ? NAV_BAR_HEIGHT : 0;

      const TAB_NAV_HEIGHT = tabbarH + navbarH;

      this.setState({
        tabBarNavBarHeight: TAB_NAV_HEIGHT,
      });
  };


  render() {
    const {
      tabBar,
      navBar,
      title,
      titleStyle,
      icon,
      visibleIcon,
      iconStyle,
      back,
      style,
      loadding
    } = this.props;
    const { tabBarNavBarHeight } = this.state;

    const navBarProps: NavBarProps = {
      title,
      titleStyle,
      icon,
      visibleIcon,
      iconStyle,
      back,
      style,
    };
    return (
      <View className={styles.main}>
        {navBar && <NavBar {...navBarProps}></NavBar>}
        {loadding&&<AtActivityIndicator mode='center' size={48} ></AtActivityIndicator>}
        {!loadding&&<View
          className={styles.container}
          style={{
            height: `calc(100% - ${tabBarNavBarHeight}px)`,
            // height: height,
            overflowY: "auto",
          }}
        >
          {this.props.children}
        </View>}

        {tabBar && <TabBar></TabBar>}
      </View>
    );
  }
}

export default Layout;
