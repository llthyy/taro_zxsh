import React from "react";

class BaseComponents<P = {}, S = {}> extends React.Component<P, S> {}

export default BaseComponents;
