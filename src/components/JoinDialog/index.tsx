import { BaseComponents, TabBar, NavBar } from "@/components";
import { View, Button,Image,Text } from "@tarojs/components";
import { AtCurtain } from 'taro-ui'
import Taro from "@tarojs/taro";
import "./index.scss"
import img_keep from "@/images/common/img_keep.png"



import styles from "./index.module.scss";

interface Props  {
  isOpened?: boolean;
  onCancel?: (e: React.MouseEvent) => void;
}

interface State {
  getAds:any[]
}

class JoinDialog extends BaseComponents<Props, State> {
  static defaultProps = {

  };

  constructor(props) {
    super(props);

    this.state = {
      getAds:[]
    };
  }

  componentDidMount() {

  }
  componentDidShow() {

  }

  close(e) {
    this.props.onCancel!(e);
  }



  render() {
    const {isOpened } = this.props;
    // const {getAds  } = this.state;
    const getAds=Taro.getStorageSync("getAds")?Taro.getStorageSync("getAds"):[]
    return (
      <AtCurtain
      isOpened={isOpened&&getAds.length>0}
      onClose={this.close.bind(this)}
    >
      {getAds.length>0&&getAds[3]&&
      <View className={styles.bigBox}>
      <View className={styles.firstRow}>参与成功</View>
      {/* <View className={styles.secondRow}>奖励领取，意见反馈发现更多精彩</View> */}
    <View className={styles.weNumber}>
      <Image src={getAds[3].ads[0].adLogo}></Image>
      <View>{getAds[3].ads[0].content}</View>
      <View className={styles.lucky}>{getAds[3].ads[0].name}祝你中大奖</View>
    </View>
      <Button className={styles.batton} onClick={e => this.close(e)}>确定</Button>
    </View>
      }

    </AtCurtain>
    );
  }
}

export default JoinDialog;
