import BaseComponents from "@/components/BaseComponents";
import TabBar from "@/components/TabBar";
import Layout from "@/components/Layout";
import NavBar from "@/components/NavBar";
import IconFont from "@/components/IconFont";
import JoinDialog from "@/components/JoinDialog";
import WinDialog from "@/components/WinDialog";
import GetRiceDialog from "@/components/GetRiceDialog";
export {BaseComponents,IconFont,TabBar, Layout, NavBar,JoinDialog ,GetRiceDialog,WinDialog};