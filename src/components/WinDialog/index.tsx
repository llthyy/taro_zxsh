import { BaseComponents, TabBar, NavBar } from "@/components";
import { View, Button, Image, Text } from "@tarojs/components";
import { AtCurtain } from 'taro-ui'
import './index.scss'
import Taro from "@tarojs/taro";
import img_keep from "@/images/common/img_keep.png"
import { getKuFuInfoApi } from '@/api';


import styles from "./index.module.scss";

interface Props {
  isOpened?: boolean;
  onCancel?: (e: React.MouseEvent) => void;
}

interface State {
  KuFuInfo:any[]
}

class WinDialog extends BaseComponents<Props, State> {
  static defaultProps = {

  };

  constructor(props) {
    super(props);

    this.state = {
      KuFuInfo:[]
    };
  }

  componentDidMount() {
    getKuFuInfoApi().then((res:any)=>{
      this.setState({KuFuInfo:res});
    })
  }
  componentDidShow() {

  }

  close(e) {
    this.props.onCancel!(e);
  }

  copyContent() {
    Taro.setClipboardData({
      data: this.state.KuFuInfo[0].value,
      success: function () {
        Taro.showToast({
          title: '复制成功',
          duration: 1500
        })
      }
    })
  }

  render() {
    const { isOpened } = this.props;
    const {KuFuInfo } = this.state;
    return (
      <AtCurtain
        isOpened={isOpened}
        onClose={this.close.bind(this)}
      >
          <View className={styles.bigBox}>
            <View className={styles.title}>恭喜您中奖！</View>
            <View className={styles.title}>快去联系客服领奖吧</View>
            <View className={styles.weNum}>
            微信号：{KuFuInfo.length>0&&KuFuInfo[0].value}
            </View>
            <Button className={styles.batton} onClick={() => this.copyContent()}>复制微信</Button>
          </View>


    </AtCurtain>
    );
  }
}

export default WinDialog;
