import React from "react";
import { Text, CoverView } from "@tarojs/components";

import "@/assets/style/iconfont.css";
import "./index.scss";

export type iconType =
  | "iconicon_agree_12px"
  | "iconicon_vip_14px"
  | "iconicon_deliver_20px"
  | "iconicon_order_20px"
  | "iconicon_payment_20px"
  | "iconicon_box_20px"
  | "iconicon_delete_14px"
  | "iconicon_round_12px"
  | "iconicon_edit_14px"
  | "iconicon_cancel_14px"
  | "iconicon_verify_16px"
  | "iconicon_phone_16px"
  | "iconicon_ldelivery_done_16"
  | "iconicon_map_15"
  | "iconicon_minus_9px"
  | "iconicon_plus_9px"
  | "iconicon_user_more_10px"
  | "iconicon_close_share_42px"
  | "iconicon_copy_24px"
  | "iconicon_sel_20px"
  | "iconicon_alipay_24px"
  | "iconicon_close_bullet_14px"
  | "iconicon_nav_back_18"
  | "iconicon_home_user_sel_24"
  | "iconicon_home_user_nor_24"
  | "iconicon_home_home_sel_24"
  | "iconicon_home_home_nor_24";

interface Props {
  icon: iconType;
  style?: React.CSSProperties;
}

/**
 *
 * @description 不要再 `CoverView` 中使用
 */
const IconFont = (props: Props) => {
  const { icon, style } = props;
  return <Text style={style} className={`iconfont_mall ${icon}`}></Text>;
};

export default IconFont;
