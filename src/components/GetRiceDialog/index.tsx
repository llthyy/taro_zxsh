import { BaseComponents, TabBar, NavBar } from "@/components";
import { View, Button,Image,Text } from "@tarojs/components";
import { AtCurtain } from 'taro-ui'
import "./index.scss"
import Taro from "@tarojs/taro";
import img_keep from "@/images/common/img_keep.png"
import rice from "@/images/home/icon_rice.png"
import img_ribbon from "@/images/home/img_ribbon.png"

import styles from "./index.module.scss";

interface Props  {
  isOpened?: boolean;
  loaddingButton?: boolean;
  adsInfo?:any;
  getCrite: () => void;
  onCancel?: (e: React.MouseEvent) => void;
}

interface State {

}

class GetRiceDialog extends BaseComponents<Props, State> {
  static defaultProps = {

  };

  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentDidMount() {

  }

  close(e) {
    this.props.onCancel!(e);
  }

  getCrite(){
    this.props.getCrite!();
  }

  render() {
    const {isOpened,adsInfo,getCrite,loaddingButton } = this.props;
    const {  } = this.state;
    return (
      <AtCurtain
        isOpened={isOpened}
        onClose={this.close.bind(this)}
      >
        <Image src={img_ribbon} className={styles.img_ribbon}></Image>
        {adsInfo.ads&&<View className={styles.bigBox}>
          <Image src={adsInfo.ads[0].adLogo}></Image>
          <View className={styles.firstRow}>{adsInfo.ads[0].content}</View>
        <View className={styles.weNumber}>
          <View>{adsInfo.ads[0].name}赠送您</View>
          <View className={styles.rice}>
          {adsInfo.ads[0].credits}
            <Image src={rice}></Image>
          </View>
        </View>
          <Button className={styles.batton} onClick={()=>this.getCrite()} loading={loaddingButton} disabled={loaddingButton}>{loaddingButton?null:"马上领取"}</Button>
        </View>}
      </AtCurtain>
    );
  }
}

export default GetRiceDialog;
