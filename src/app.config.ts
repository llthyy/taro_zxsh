export default {
  entryPagePath: "pages/index/index",
  pages: [
    'pages/index/index',
    'pages/my/index',
    'pages/login/index',
    'pages/treasureDetail/index',
    'pages/lotteryDetail/index',
    'pages/rule/treasureRule/index',
    'pages/rule/lotteryRule/index',
    'pages/commonProblem/index',
    'pages/myLottery/index',
    'pages/lotteryUserList/index',
    'pages/webView/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
    navigationStyle: "custom",
  },
  tabBar: {
    custom: true,
    list: [
      {
        pagePath: "pages/index/index",
        text: "首页",
      },
      {
        pagePath: "pages/my/index",
        text: "我的",
      },
    ],
    color: "#000",
    selectedColor: "#56abe4",
    backgroundColor: "#fff",
    borderStyle: "white",
  },
  permission: {
    "scope.userLocation": {
      "desc": "你的位置信息将用于小程序位置接口的效果展示"
    }
  }
}
