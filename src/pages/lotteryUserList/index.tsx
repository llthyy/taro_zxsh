
import { View, Text, Image, Button } from '@tarojs/components'
import styles from "./index.module.scss";
import Taro from "@tarojs/taro";
import { AtButton, AtList, AtListItem, AtProgress, AtCountdown } from 'taro-ui'
import { BaseComponents, Layout } from "@/components";
import {timestampToTime} from "@/utils";
import { lotteryUserListApi } from '@/api/detail';






interface Props { }
interface State {
  lotteryUserList: any[];
  loadding:boolean
}

export default class Index extends BaseComponents<Props, State> {
  public code = Taro.getCurrentInstance().router.params.code;
  constructor(props: Props) {
    super(props);

    this.state = {
      lotteryUserList: [],
      loadding:false
    };
  }

  componentWillMount() { }

  componentDidMount() {
    this.setState({ loadding: true });
  }

  componentWillUnmount() { }

  componentDidShow() {
    // console.log(timestampToTime('2021-06-05 12:39:12'))
    lotteryUserListApi(1, 10, this.code).then((res: any) => {
      this.setState({ lotteryUserList: res.records });
      this.setState({ loadding: false });
    })
  }

  componentDidHide() { }



  render() {
    const { lotteryUserList,loadding } = this.state;
    return (
      <Layout navBar title="本期参与" visibleIcon loadding={loadding}>
        <View className={styles.index}>
          {lotteryUserList.length>0&&lotteryUserList.map((item,index)=>{
            return <View className={styles.item} key={index}>
            <View className={styles.item_left}>
              <Image src={item.avatar}></Image>
              <Text>{item.alias}</Text>
            </View>
            <View className={styles.item_right}>{timestampToTime(item.modifiedOn)}</View>
          </View>
          })}

        </View>
      </Layout>
    )
  }
}
