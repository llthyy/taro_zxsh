
import { View, Text, Image, Button,WebView } from '@tarojs/components'
import styles from "./index.module.scss";
import Taro, { getCurrentInstance } from '@tarojs/taro'

import { BaseComponents, Layout, GetRiceDialog } from "@/components";






interface Props { }
interface State {
  webUrl:string;
}

export default class Index extends BaseComponents<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      webUrl:""
    };
  }

  componentWillMount() {

  }

  componentDidMount() {
    const webUrl = Taro.getCurrentInstance().router.params.webUrl;
    if(webUrl){
      this.setState({webUrl});
    }
  }

  componentWillUnmount() { }

  componentDidShow() {


  }

  componentDidHide() { }





  render() {
    const { webUrl } = this.state;
    return (
      <Layout navBar title="" visibleIcon loadding={false}>
        <View className={styles.index}>
          <WebView className={styles.webView} src={webUrl}></WebView>
        </View>
      </Layout>
    )
  }
}
