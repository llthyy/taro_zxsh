
import { View, Text, Image, Button } from '@tarojs/components'
import styles from "./index.module.scss";
import "./index.scss";
import { AtCurtain } from 'taro-ui'
import Taro, { clearStorage } from "@tarojs/taro";
import { isLogin } from '@/utils/common'
import { BaseComponents, Layout } from "@/components";
import banner_bg from "@/images/my/img_my_bg.png"
import icon_exit from "@/images/my/icon_exit.png"
import headerDefault from "@/images/home/header_photo_default.png"
import rice from "@/images/my/icon_rice.png"
import arrow_right from "@/images/my/icon_arrow_right.png"
import icon_service from "@/images/my/icon_service.png"
import icon_blank from "@/images/my/icon_blank.png"
import icon_arrows from "@/images/my/icon_arrows.png"
import icon_contact from "@/images/my/icon_contact.png"
import icon_diamond from "@/images/my/icon_diamond.png"
import icon_gift from "@/images/my/icon_gift.png"
import icon_score from "@/images/my/icon_score.png"
import icon_create from "@/images/my/icon_create.png"

import { getUserinfoApi, getKuFuInfoApi, getSoupInfoApi } from '@/api';

interface Props { }
interface State {
  isOpened: boolean;
  userInfo: any;
  loadding: boolean;
  KuFuInfo: any[];
  soupInfo: any[]
}

export default class Index extends BaseComponents<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isOpened: false,
      userInfo: {},
      loadding: false,
      KuFuInfo: [],
      soupInfo: []
    };
  }

  componentWillMount() {
    this.setState({ loadding: true });
  }

  componentDidMount() {

    getKuFuInfoApi().then((res: any) => {
      this.setState({ KuFuInfo: res });
    })
    this.getSoupInfo();
  }

  componentWillUnmount() { }

  componentDidShow() {
    if (Taro.getStorageSync('userInfo')) {
      getUserinfoApi().then((res) => {
        Taro.setStorageSync('userInfo', res);
        this.setState({ loadding: false });
        this.setState({ userInfo: res })
      })
    } else {
      this.setState({ loadding: false });
    }
    // if(!Taro.getStorageSync("token")){
    //   Taro.navigateTo({
    //     url: '/pages/login/index'
    //   })
    // }
  }

  componentDidHide() { }

  //下拉时触发的函数
  onPullDownRefresh() {
    let pages = Taro.getCurrentPages();;
    let currPage: any = null;
    if (pages.length) {
      currPage = pages[pages.length - 1];
    };
    currPage.onShow();
    Taro.stopPullDownRefresh();
  }

  //获取心灵鸡汤
  getSoupInfo = () => {
    getSoupInfoApi().then((res: any) => {
      if (res) {
        this.setState({ soupInfo: res });
      }
    })
  }

  goLottery = (type) => {
    if (isLogin()) { Taro.navigateTo({ url: `/pages/myLottery/index?type=${type}` }); }
  }
  goProblem = () => {
    Taro.navigateTo({ url: "/pages/commonProblem/index" });
  }

  handleChange() {
    this.setState({
      isOpened: true
    })
  }
  onClose() {
    this.setState({
      isOpened: false
    })
  }
  copyContent() {
    Taro.setClipboardData({
      data: this.state.KuFuInfo[0].value,
      success: function () {
        Taro.showToast({
          title: '复制成功',
          duration: 1500
        })
      }
    })
  }
  exit() {
    Taro.clearStorageSync();
    this.setState({ userInfo: {} });
  }





  render() {
    const { isOpened, userInfo, loadding, KuFuInfo, soupInfo } = this.state;
    return (
      <Layout tabBar title="个人中心" loadding={loadding}>
        <View className={styles.index}>
          <View className={styles.banner}>
            <Image src={banner_bg}></Image>
          </View>

          <View className={styles.personInfo}>
            <View className={styles.headPhoto} onClick={() => { isLogin() }}>
              <Image src={userInfo.avatar ? userInfo.avatar : headerDefault} ></Image>
            </View>
            <Text className={styles.userName} onClick={() => { isLogin() }}>{userInfo.alias ? userInfo.alias : "欢迎登录"}</Text>
            {JSON.stringify(userInfo) != "{}" &&
              <View className={styles.exitButton} onClick={() => this.exit()}>
                <Text>退出</Text>
                <Image src={icon_exit}></Image>
              </View>}

            <Text className={styles.saying}>{soupInfo.length > 0 && soupInfo[0].content}</Text>
            <View className={styles.demarcation}></View>
            <View className={styles.rice}>
              <Text>米粒：</Text>
              <Text className={styles.riceNumber}>{userInfo.credits ? userInfo.credits : 0}</Text>
              <Image src={rice}></Image>
            </View>
          </View>

          <View className={styles.middle}>
            <View className={styles.myItem} onClick={() => this.goLottery("treasure")}>
              <Image src={icon_diamond}></Image>
              <View>我的夺宝</View>
            </View>
            <View className={styles.myItem} onClick={() => this.goLottery("lottery")}>
              <Image src={icon_gift}></Image>
              <View>我的抽奖</View>
            </View>
            <View className={styles.myItem} onClick={() => {}}>
              <Image src={icon_score}></Image>
              <View>分享赚积分</View>
            </View>
            <View className={styles.myItem} onClick={() => {}}>
              <Image src={icon_create}></Image>
              <View>创建抽奖</View>
            </View>
          </View>
          <View className={styles.bottom}>
            <View className={styles.item} onClick={() => this.goProblem()}>
              <View className={styles.item_right}>
                <Image src={icon_service}></Image>
                <Text>常见问题</Text>
              </View>
              <Image src={icon_arrows}></Image>
            </View>
            <View className={styles.item} onClick={this.handleChange.bind(this)}>
              <View className={styles.item_right}>
                <Image src={icon_blank}></Image>
                <Text>联系客服</Text>
              </View>
              <Image src={icon_arrows}></Image>
            </View>
          </View>
        </View>
        {/* 联系客服弹框 */}
        <AtCurtain
          isOpened={isOpened}
          onClose={this.onClose.bind(this)}
        >
          <View className={styles.bigBox}>
            <View className={styles.firstRow}>添加客服微信</View>
            <View className={styles.secondRow}>奖励领取，意见反馈发现更多精彩</View>
            <View className={styles.weNumber}>
              <Image src={icon_contact}></Image>
              <View>微信号：{KuFuInfo.length > 0 && KuFuInfo[0].value}</View>
            </View>
            <Button className={styles.batton} onClick={() => { this.copyContent() }}>复制微信</Button>
          </View>
        </AtCurtain>
      </Layout>
    )
  }
}
