
import { View, Text, Image, Button } from '@tarojs/components'
import styles from "./index.module.scss";
import Taro from "@tarojs/taro";
import { AtButton, AtList, AtListItem, AtProgress, AtCountdown } from 'taro-ui'
import { BaseComponents, Layout } from "@/components";
import { myLotteryApi, myTreasureApi } from '@/api/myLottery';

import winning from "@/images/common/winning.png"
import no_winning from "@/images/common/no_winning.png"




interface Props { }
interface State {
  lotteryList: any[];
  loadding: boolean;
}

export default class Index extends BaseComponents<Props, State> {
  public type = Taro.getCurrentInstance().router.params.type;
  constructor(props: Props) {
    super(props);

    this.state = {
      lotteryList: [],
      loadding:false
    };
  }

  componentWillMount() { }

  componentDidMount() { this.setState({ loadding: true });}

  componentWillUnmount() { }

  componentDidShow() {
    const userInfo = Taro.getStorageSync("userInfo");
    if (this.type == "lottery") {
      myLotteryApi(1, 10, userInfo.code).then((res: any) => {
        if (res) {
          this.setState({ lotteryList: res.records });
          this.setState({ loadding: false });
        }
      });
    } else {
      myTreasureApi(1, 10, userInfo.code).then((res: any) => {
        if (res) {
          this.setState({ lotteryList: res.records });
          this.setState({ loadding: false });
        }
      });
    }

  }

  componentDidHide() { }

  goDetail(code) {
    if (this.type == "lottery"){
      Taro.navigateTo({
        url: `/pages/lotteryDetail/index?code=${code}`
      });
    } else {
      Taro.navigateTo({
        url: `/pages/treasureDetail/index?code=${code}`
      });
    }
  }


  // status 待开奖： 0,未中奖： 1, 中奖: 2
  render() {
    const { lotteryList,loadding } = this.state;
    return (
      <Layout navBar title="我的抽奖" visibleIcon loadding={loadding}>
        <View className={styles.index}>
          {lotteryList.length > 0 && lotteryList.map((item, index) => {
            return <View className={styles.item} key={index} onClick={() => this.goDetail(item.code)}>
              <View className={styles.item_img}>
                <Image src={item.bannerImg}></Image>
              </View>
              <View className={styles.content}>
                {/* <View className={styles.contentTop}>
                  <View>{item.name}</View>
                  <View></View>
                </View> */}
                <View className="at-row at-row__justify--between">
                    <View className='at-col at-col-5'>
                    <View className={styles.name}>{item.name}</View>
                    </View>
                    <View className='at-col at-col-5' style={{"textAlign":"right"}}>
                      <Text className={styles.number}>x {item.giftNum}</Text>
                    </View>
                  </View>
                <View className={styles.price}>¥ {item.originalPrice}</View>
              </View>
              {item.status == "1" && <Image src={winning}></Image>}
              {item.status == "2" && <Image src={winning}></Image>}
            </View>
          })}
        </View>
      </Layout>
    )
  }
}
