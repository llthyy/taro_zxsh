
import { View, Text, Image, Button } from '@tarojs/components'
import styles from "./index.module.scss";
import { BaseComponents, Layout } from "@/components";
import Taro from "@tarojs/taro";

interface Props { }
interface State {

}

export default class Index extends BaseComponents<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {

    };
  }

  componentWillMount() { }

  componentDidMount() {

  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }


  render() {
    const { } = this.state;
    const baseInfo:any =JSON.parse(Taro.getCurrentInstance().router.params.baseInfo);
    console.log(baseInfo)
    return (
      <Layout navBar title="夺宝规则" visibleIcon >
        <View className={styles.index}>
          <View className={styles.title}>
            <Text className={styles.title_text}>活动规则</Text>
          </View>
          <View className={styles.content}>
          1.所有用户可选择自己喜欢的商品(可多选)，支付所需的积分(米粒)(以下简称积分)参与抽 奖，获取抽奖码。
          </View>
          <View className={styles.content}>
          2.每次参与需消耗{baseInfo.credits}积分兑换一个抽奖码，积分不予退还。
          </View>
          <View className={styles.content}>
          3.本宝⻉每人最多可兑换{baseInfo.joinLimit}个抽奖码。
          </View>
          <View className={styles.content}>
          4.本宝⻉开奖所需积分总数集满当日{baseInfo.openTime&&baseInfo.openTime.split(" ")[1].split(":").splice(0,2).join(":")}开奖，开奖前半小时冻结， 如当日20:30前未集满，则顺延至下一日，直到米粒积分集满为止。
          </View>

          <View className={styles.title}>
            <Text className={styles.title_text}>中奖规则</Text>
          </View>
          <View className={styles.content}>
          1.中奖奖品请在“我的-我的夺宝”⻚面中查看。
          </View>
          <View className={styles.content}>
          2.开奖后，中奖用户需主动联系客服领取奖品，超过三天未联系客服领奖则视为自动放弃该奖品。
          </View>
          <View className={styles.content}>
          3.用户收货地址填写错误造成的奖品损失，将由用户自己承担责任。
          </View>
          <View className={styles.content}>
          4.本平台作为提供发起与参与抽奖的公共平台，活动奖品由平台或者第三方赞助商提供并发放，本 平台会在法律范围内尽可能规范督促用户遵守相关法律，营造良好的平台使用环境。
          </View>
          <View className={styles.content}>
          5.抽奖期间发生任何不可抗力事件使本抽奖活动或本次抽奖目的不能实现，本平台因此免责。不可 抗力是指不能控制、不可预⻅或不能避免，即使预⻅亦无法避免的事件，该事件使任何一方根据本 协议履行其全部或部分义务已不可能。包括社会异常事件，自然灾害或政府管制行为而造成的网络 关闭等非本平台过错的行为。
          </View>
          <View className={styles.content}>
          温馨提示:国税局有权对中奖奖品进行纳税征收，纳税是每个公⺠的基本义务。
          </View>
        </View>
      </Layout>
    )
  }
}
