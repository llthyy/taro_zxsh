
import { View, Text, Image, Button } from '@tarojs/components'
import styles from "./index.module.scss";
import { BaseComponents, Layout } from "@/components";
import Taro from "@tarojs/taro";

interface Props { }
interface State {

}

export default class Index extends BaseComponents<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {

    };
  }

  componentWillMount() { }

  componentDidMount() {

  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }


  render() {
    const { } = this.state;
    return (
      <Layout navBar title="抽奖规则" visibleIcon >
        <View className={styles.index}>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>01</Text>
            </View>
            <Text className={styles.content}>
            所有用户可选择自己喜欢的商品(可多选)，支付所需的积分(米粒)参与抽奖，获取抽奖号。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>02</Text>
            </View>
            <Text className={styles.content}>
            参与抽奖的过程中，如用户出现违规行为(如利用黑客工具，软件作弊等)，本平台有权取消用 户中奖资格。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>03</Text>
            </View>
            <Text className={styles.content}>
            每个宝⻉当日20:00开奖，开奖前半小时冻结。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>04</Text>
            </View>
            <Text className={styles.content}>
            中奖奖品请在“我的-我的夺宝”⻚面中查看。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>05</Text>
            </View>
            <Text className={styles.content}>
            本平台会在开奖结果⻚面展示中奖用户的头像和中奖资格。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>06</Text>
            </View>
            <Text className={styles.content}>
            中奖奖品请在“我的-我的抽奖”⻚面中查看。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>07</Text>
            </View>
            <Text className={styles.content}>
            开奖后，中奖用户需主动联系客服领取奖品，超过三天未联系客服领奖则视为自动放弃该奖品。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>08</Text>
            </View>
            <Text className={styles.content}>
            用户收货地址填写错误造成的奖品损失，将由用户自己承担责任。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>09</Text>
            </View>
            <Text className={styles.content}>
            本平台作为提供发起与参与抽奖的公共平台，活动奖品由平台提供或者由第三方赞助商提供并发 放，本平台会在法律的范围内尽可能规范督促用户遵守相关法律，营造良好的平台使用环境。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}>10</Text>
            </View>
            <Text className={styles.content}>
            抽奖期间发生任何不可抗力事件使本抽奖活动或本次抽奖目的不能实现，本平台因此免责。不可 抗力是指不能控制、不可预⻅或不能避免，即使预⻅亦无法避免的事件，该事件使任何一方根据本 协议履行其全部或部分义乌已不可能。包括社会异常事件，自然灾害或政府管制行为而造成的网络 关闭等非本平台过错的行为。
            </Text>
          </View>
          <View className={styles.row}>
            <View className={styles.title}>
              <Text className={styles.title_text}></Text>
            </View>
            <Text className={styles.content}>
            温馨提示:国税局有权对中奖奖品进行纳税征收，纳税是每个公⺠的基本义务。
            </Text>
          </View>
        </View>
      </Layout>
    )
  }
}
