
import { View, Text, Image, Button } from '@tarojs/components'
import styles from "./index.module.scss";
import "./index.scss";
import Taro from "@tarojs/taro";
import React, { useRef, useState, useEffect } from 'react';
import { BaseComponents, Layout } from "@/components";




interface Props { }
interface State {
  list: any[];
}


export default class Index extends BaseComponents<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      list: [
        { title: "如何获得积分？", content: "和真香优选的外卖合作商家都会在您的外卖中放入卡片，每次扫卡片上面小程序码即可获得积分奖励。", needMore: false, isMore: false },
        { title: "如何获得积分？", content: "和真香优选的外卖合作商家都会在您的外卖中放入卡片，每次扫卡片上面小程序码即可获得积分奖励。和真香优选的外卖合作商家都会在您的外卖中放入卡片，每次扫卡片上面小程序码即可获得积分奖励。", needMore: false, isMore: false },
        { title: "如何获得积分？", content: "和真香优选的外卖合作商家都会在您的外卖中放入卡片。", needMore: false, isMore: false },
      ],

    };
  }

  componentWillMount() { }

  componentDidMount() {
    setTimeout(() => {
      const query = Taro.createSelectorQuery()
      query.selectAll('.content').boundingClientRect((rec: any) => {
        rec.map((item, index) => {
          if (item.height > 70) {
            let arr = JSON.parse(JSON.stringify(this.state.list))
            arr[index].needMore = true;
            this.setState({ list: arr })
          }
        })
      }).exec()
    }, 50)

  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  isMore = (index) => {
    let arr = JSON.parse(JSON.stringify(this.state.list))
    arr[index].isMore = !this.state.list[index].isMore;
    this.setState({ list: arr });
  }



  render() {
    const { list } = this.state;
    return (
      <Layout navBar title="常见问题" visibleIcon>
        <View className={styles.index}>
          {list.length > 0 && list.map((item, index) => {
            return <View className={styles.item} key={index}>
              <View className={styles.title} >{item.title}</View>
              {item.needMore?
              <Text className={ item.isMore ? "showContent" : "moreContent"} >{item.content}</Text>:
              <Text className={"content"} >{item.content}</Text>}


              {/* <Content content={item.content}></Content> */}
              {item.needMore && <View className={styles.showOrHide} onClick={() => this.isMore(index)}>{item.isMore?"收起":"查看更多"}</View>}
            </View>
          })}

        </View>
      </Layout>
    )
  }
}

