import Request from '@/utils/request'
//wxb6ff62530b5928c0
export async function lotteryListApi() {
  return await Request.get("/lottery/lottery1List")
}

export async function treasureListApi() {
  return await Request.get("/lottery/lottery2List")
}

export async function winListApi(pageNum:number,pageSize:number) {
  return await Request.get(`/lottery/user/winListPage?pageNum=${pageNum}&pageSize=${pageSize}`)
}


//获取广告
export async function getAdsApi(scene?:string) {
  if(scene){
    return await Request.get(`/ad/position/getAds?scene=${scene}`)
  }else{
    return await Request.get(`/ad/position/getAds`)
  }
}
//领取积分
export async function getCreditApi(paramas:object) {
    return await Request.post('/wx/user/credits/scanQrAdd',paramas)
}