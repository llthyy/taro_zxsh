import Request from '@/utils/request'
//wxb6ff62530b5928c0
export async function loginApi(data:object) {
  return await Request.post("/wx/user/login",data)
}
export async function updateUserinfoApi(data:object) {
  return await Request.post("/wx/user/update",data)
}

//获取用户信息
export async function getUserinfoApi() {
  return await Request.get("/wx/user/getCurrentUser")
}
//获取客服信息
export async function getKuFuInfoApi() {
  return await Request.get("/dict/getByType/KeFu")
}

//获取心灵鸡汤信息
export async function getSoupInfoApi() {
  return await Request.get("/dict/soup/getTodaySoup")
}