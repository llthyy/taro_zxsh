import Request from '@/utils/request'

//我的抽奖
export async function myLotteryApi(pageNum:number,pageSize:number,userCode:string) {
    return await Request.get(`/lottery/user/lottery1List?pageNum=${pageNum}&pageSize=${pageSize}&userCode=${userCode}`)
  }
export async function myTreasureApi(pageNum:number,pageSize:number,userCode:string) {
    return await Request.get(`/lottery/user/lottery2List?pageNum=${pageNum}&pageSize=${pageSize}&userCode=${userCode}`)
  }

