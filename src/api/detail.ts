import Request from '@/utils/request'

export async function lotteryDetailApi(code,userCode) {
  return await Request.get(`/lottery/lottery1Detail?code=${code}&userCode=${userCode}`)
}
export async function treasureDetailApi(code,userCode) {
  return await Request.get(`/lottery/lottery2Detail?code=${code}&userCode=${userCode}`)
}

// 用户参与抽奖
export async function lotteryJoinApi(paramas:object) {
    return await Request.post("/lottery/user/join",paramas)
  }
export async function treasureJoinApi(paramas:object) {
    return await Request.post("/lottery/user/join",paramas)
  }

//抽奖用户列表
export async function lotteryUserListApi(pageNum,pageSize,lotteryCode) {
    return await Request.get(`/lottery/user/listPage?pageNum=${pageNum}&pageSize=${pageSize}&lotteryCode=${lotteryCode}`)
  }
